////
////  VPNService.swift
////  TrustZoneVPN
////
////  Created by Konstantin Chukhas on 8/18/19.
////  Copyright © 2019 Konstantin Chukhas. All rights reserved.
////
//
//import Foundation
//import UIKit
//import NetworkExtension
//import KeychainSwift
//
//class VPNService{
//    static let sharedInstance = VPNService()
//    var vpnManager = NEVPNManager.shared()
//    var isConnected = false
//    private let keychain = KeychainService()
//    init() {
//         keychain.save(key: "vpn_password", value: UserDefaults.standard.object(forKey: "password") as! String)
//    }
//    
//    func initVPNTunnelProviderManager(){
//        
//        self.vpnManager.loadFromPreferences { [passwordRef = keychain.load(key: "vpn_password")] (error) -> Void in
//            
//            if((error) != nil) {
//                print("VPN Preferences error: 1")
//            }
//            else {
//                
//                // You can change Protocol and credentials as per your protocol i.e IPSec or IKEv2
//                let p = NEVPNProtocolIKEv2()
//                p.username = (UserDefaults.standard.object(forKey: "login") as! String)
//                p.remoteIdentifier = "trust.zone"
//                p.serverAddress =  UserDefaults.standard.object(forKey: "domen") as? String ?? "vpn.trust.zone"
//                p.useExtendedAuthentication = true
//                p.disconnectOnSleep = false
//                p.passwordReference = passwordRef
//                p.authenticationMethod = .none
//                p.enablePFS = true
//                
//                p.sharedSecretReference = passwordRef
//                // Useful for when you have IPSec Protocol
//                
//                self.vpnManager.protocolConfiguration = p
//                self.vpnManager.isEnabled = true
//                
//                self.vpnManager.saveToPreferences(completionHandler: { (error) -> Void in
//                    if((error) != nil) {
//                        print("VPN Preferences error: 2")
//                    }
//                    else {
//                        
//                        
//                        self.vpnManager.loadFromPreferences(completionHandler: { (error) in
//                            
//                            if((error) != nil) {
//                                
//                                print("VPN Preferences error: 2")
//                            }
//                            else {
//                                
//                                var startError: NSError?
//                                
//                                do {
//                                    try self.vpnManager.connection.startVPNTunnel()
//                                }
//                                catch let error as NSError {
//                                    startError = error
//                                    print(startError)
//                                }
//                                catch {
//                                    print("Fatal Error")
//                                    fatalError()
//                                }
//                                if((startError) != nil) {
//                                    print("VPN Preferences error: 3")
//                                    let alertController = UIAlertController(title: "Oops..", message:
//                                        "Something went wrong while connecting to the VPN. Please try again.", preferredStyle: UIAlertController.Style.alert)
//                                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default,handler: nil))
//                                    
//                                    
//                                    
////                                    self.present(alertController, animated: true, completion: nil)
//                                    print(startError)
//                                }
//                                else {
//                                    self.VPNStatusDidChange(nil)
//                                    
//                                    print("VPN started successfully..")
//                                }
//                                
//                            }
//                            
//                        })
//                        
//                    }
//                })
//            }
//        }
//    }
//    func switchClicked() {
//        
//        //HomeVC.shared.switchConntectionStatus.isOn = false
//        
//        if !isConnected {
//            initVPNTunnelProviderManager()
//        }
//        else{
//            vpnManager.removeFromPreferences(completionHandler: { (error) in
//                
//                if((error) != nil) {
//                    print("VPN Remove Preferences error: 1")
//                }
//                else {
//                    HomeVC.shared.vpnManager.connection.stopVPNTunnel()
//                    HomeVC.shared.labelConntectionStatus.text = "Disconnected"
//                    HomeVC.shared.switchConntectionStatus.isOn = false
//                    HomeVC.shared.isConnected = false
//                }
//            })
//        }
//    }
//    
//    @objc func VPNStatusDidChange(_ notification: Notification?) {
//        
//        print("VPN Status changed:")
//        let status = self.vpnManager.connection.status
//        switch status {
//        case .connecting:
//            print("Connecting...")
//         HomeVC.shared.labelConntectionStatus.text = "Connecting..."
//            
//                 HomeVC.shared.isConnected = false
//            UIView.animate(withDuration: 0.5, animations: {() -> Void in
//                let yourImage: UIImage = UIImage(named: "ic_launcher")!
//                     HomeVC.shared.img.image = yourImage
//                     HomeVC.shared.img?.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
//                UserDefaults.standard.set("ic_launcher", forKey: "ImageDefaults")
//            })
//            let nameLogo  =  "ic_launcher"
//            UserDefaults.standard.set(nameLogo, forKey: "logo")
//            UserDefaults.standard.set(false, forKey: "vpnStatus")
//            
//            
//            //gradient
//                 HomeVC.shared.materialActivityIndicator.addGradientWithColor(color: #colorLiteral(red: 0.2509803922, green: 0.7529411765, blue: 1, alpha: 1))
//            HomeVC.shared.materialActivityIndicator.layer.cornerRadius = 125
//            HomeVC.shared.materialActivityIndicator.startAnimating()
//            HomeVC.shared.materialActivityIndicator.rotate(duration: 3.0)
//            
//            HomeVC.shared.materialActivityIndicator.color = #colorLiteral(red: 0.3103698492, green: 0.7292534709, blue: 0.9948167205, alpha: 1)
//            HomeVC.shared.materialActivityIndicator.lineWidth =  90
//            HomeVC.shared.materialActivityIndicator.layer.cornerRadius = 125
//            HomeVC.shared.materialActivityIndicator.isHidden = false
//            HomeVC.shared.touchLabel.text = "Touch to leave Trust.Zone"
//            
//            break
//        case .connected:
//            print("Connected")
////                 HomeVC.shared.labelConntectionStatus.text = "Connected"
////                 HomeVC.shared.switchConntectionStatus.isOn = true
////                 HomeVC.shared.isConnected = true
////            HomeVC.shared.materialActivityIndicator.stopAnimating()
////            UIView.animate(withDuration: 0.5, animations: {() -> Void in
////                let yourImage: UIImage = UIImage(named: "logo_done")!
////                     HomeVC.shared.img.image = yourImage
////                let imgData = yourImage.jpegData(compressionQuality: 1)
////                     HomeVC.shared.img?.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
////                UserDefaults.standard.set(imgData, forKey: "ImageDefaults")
////            })
////            let nameLogo  = "logo_done"
////            UserDefaults.standard.set(nameLogo, forKey: "logo")
////            UserDefaults.standard.set(true, forKey: "vpnStatus")
////
////
////            //                UserDefaults.standard.set(UIImageJPEGRepresentation(image, 100), forKey: "key")
////
////            HomeVC.shared.materialActivityIndicator.isHidden = true
////            HomeVC.shared.touchLabel.text = "Touch to leave Trust.Zone"
////            NetworkManager.sharedInstance.aa { (result) in
////                     HomeVC.shared.servLine.append(result! as! String)
////                for line in      HomeVC.shared.servLine {
////                    let serverProps = line.components(separatedBy: "|")
////                    if serverProps.count < 1 { continue }
////                    HomeVC.shared.ipLabel.text = serverProps[0]
////                    let image: UIImage = UIImage(named:"\(serverProps[1].lowercased())flags")!
////                    HomeVC.shared.ipImage.image = image
////                }
////            }
////            VPNMethod.share.vpnStatusImage = true
//            
//            break
//        case .disconnecting:
//            print("Disconnecting...")
//                 HomeVC.shared.labelConntectionStatus.text = "Disconnecting..."
//                 HomeVC.shared.switchConntectionStatus.isOn = false
//                 HomeVC.shared.isConnected = false
//            HomeVC.shared.materialActivityIndicator.stopAnimating()
//            UIView.animate(withDuration: 0.5, animations: {() -> Void in
//                let yourImage: UIImage = UIImage(named: "ic_launcher")!
//                     HomeVC.shared.img.image = yourImage
//                     HomeVC.shared.img?.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
//                UserDefaults.standard.set("ic_launcher", forKey: "ImageDefaults")
//            })
//            HomeVC.shared.materialActivityIndicator.isHidden = true
//            UserDefaults.standard.set(false, forKey: "vpnStatus")
//            
//            let nameLogo  = "ic_launcher"
//            UserDefaults.standard.set(nameLogo, forKey: "logo")
//            break
//        case .disconnected:
//            print("Disconnected")
//                 HomeVC.shared.labelConntectionStatus.text = "Disconnected..."
//                 HomeVC.shared.switchConntectionStatus.isOn = false
//                 HomeVC.shared.isConnected = false
//            HomeVC.shared.materialActivityIndicator.stopAnimating()
//            UIView.animate(withDuration: 0.5, animations: {() -> Void in
//                let yourImage: UIImage = UIImage(named: "logo_fail")!
//                     HomeVC.shared.img.image = yourImage
//                     HomeVC.shared.img?.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
//                UserDefaults.standard.set("logo_fail", forKey: "ImageDefaults")
//            })
//            VPNMethod.share.vpnStatusImage = false
//            UserDefaults.standard.set(false, forKey: "vpnStatus")
//            
//            let nameLogo  = "logo_fail"
//            UserDefaults.standard.set(nameLogo, forKey: "logo")
//            HomeVC.shared.materialActivityIndicator.isHidden = true
//            HomeVC.shared.touchLabel.text = "Touch to enter Trust.Zone"
//            NetworkManager.sharedInstance.aa { (result) in
//                     HomeVC.shared.servLine.append(result! as! String)
//                for line in      HomeVC.shared.servLine {
//                    let serverProps = line.components(separatedBy: "|")
//                    if serverProps.count < 1 { continue }
//                    HomeVC.shared.ipLabel.text = serverProps[0]
//                    let image: UIImage = UIImage(named:"\(serverProps[1].lowercased())flags")!
//                    HomeVC.shared.ipImage.image = image
//                }
//            }
//            VPNMethod.share.vpnStatusImage = false
//            
//            if UserDefaults.standard.bool(forKey: "trueAutoconnect")  == true {
//                HomeVC.shared.switchClicked()
//                HomeVC.shared.materialActivityIndicator.addGradientWithColor(color: #colorLiteral(red: 0.2509803922, green: 0.7529411765, blue: 1, alpha: 1))
//                HomeVC.shared.materialActivityIndicator.layer.cornerRadius = 125
//                HomeVC.shared.materialActivityIndicator.startAnimating()
//                HomeVC.shared.materialActivityIndicator.rotate(duration: 3.0)
//                
//                HomeVC.shared.materialActivityIndicator.color = #colorLiteral(red: 0.3103698492, green: 0.7292534709, blue: 0.9948167205, alpha: 1)
//                HomeVC.shared.materialActivityIndicator.lineWidth =  90
//                HomeVC.shared.materialActivityIndicator.layer.cornerRadius = 125
//                HomeVC.shared.materialActivityIndicator.isHidden = false
//            }
//            
//            break
//        case .invalid:
//            print("Invalid")
////            vpnManager.connection.stopVPNTunnel()
//////                 HomeVC.shared.labelConntectionStatus.text = "Invalid Connection"
//////                 HomeVC.shared.switchConntectionStatus.isOn = false
//////                 HomeVC.shared.isConnected = false
//////            HomeVC.shared.materialActivityIndicator.stopAnimating()
////            UIView.animate(withDuration: 0.5, animations: {() -> Void in
////                let nlogo = UserDefaults.standard.object(forKey: "logo")
////                let yourImage: UIImage =  UIImage(named: nlogo as? String ?? "ic_launcher")!
////                //                let yourImage: UIImage = UIImage(named: "ic_launcher")!
////                   //  HomeVC.shared.img.image = yourImage
////                    // HomeVC.shared.img?.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
////                UserDefaults.standard.set("ic_launcher", forKey: "ImageDefaults")
////            })
////            let nameLogo  = "ic_launcher"
////            UserDefaults.standard.set(nameLogo, forKey: "logo")
////            HomeVC.shared.materialActivityIndicator.isHidden = true
////            HomeVC.shared.touchLabel.text = "Touch to enter Trust.Zone"
////            VPNMethod.share.vpnStatusImage = false
////            
////            NetworkManager.sharedInstance.aa { (result) in
////                     HomeVC.shared.servLine.append(result! as! String)
////                for line in      HomeVC.shared.servLine {
////                    let serverProps = line.components(separatedBy: "|")
////                    if serverProps.count < 1 { continue }
////                    HomeVC.shared.ipLabel.text = serverProps[0]
////                    let image: UIImage = UIImage(named:"\(serverProps[1].lowercased())flags")!
////                    HomeVC.shared.ipImage.image = image
////                }
////            }
////            VPNMethod.share.vpnStatusImage = false
////            UserDefaults.standard.set(false, forKey: "vpnStatus")
////            
//            break
//        case .reasserting:
//            print("Reasserting...")
//                 HomeVC.shared.labelConntectionStatus.text = "Reasserting Connection"
//                 HomeVC.shared.switchConntectionStatus.isOn = false
//                 HomeVC.shared.isConnected = false
//            VPNMethod.share.vpnStatusImage = false
//            UserDefaults.standard.set(false, forKey: "vpnStatus")
//            
//            
//            break
//        }
//    }
//}
