//
//  ServersService.swift
//  openSSlVPN
//
//  Created by Zakhar on 7/30/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import Foundation

class ServersService {
    
    static var shared = ServersService()
    
    let serversUpdatedNoticationName = Notification.Name("serversUpdated")
    
    private var isLoading = false
    
    private(set) var serverGroups: [ServerGroup] = [] {
        didSet { NotificationCenter.default.post(name: serversUpdatedNoticationName, object: nil) }
    }
    
    private init() { }
    
    /// Loads server groups in utility thread (not main thread) and stores them in inside service in main thread
    func loadServerGroups() {
        DispatchQueue.global(qos: .utility).async { [weak self] in
            guard let self = self, !self.isLoading else { return }
            self.isLoading = true
            let serersString = self.getServersString()
            let servers = self.parseServersString(serersString)
            let groups = self.groupServers(servers)
            
            if self.serverGroups != groups {
                DispatchQueue.main.async { self.serverGroups = groups }
            }
            
            self.isLoading = false
        }
    }
    
    private func groupServers(_ servers: [Server]) -> [ServerGroup] {
        var groups = [ServerGroup]()
        var groupNames = [String]()
        
        for server in servers {
            if !groupNames.contains(server.groupName) {
                groupNames.append(server.groupName)
            }
        }
        
        for groudName in groupNames {
            let groupedServers = servers.filter({ $0.groupName == groudName })
            let group = ServerGroup(name: groudName, servers: groupedServers)
            groups.append(group)
        }
        
        return groups
    }
    
    private func getServersString() -> String {
        let strsize = 10240
        let serversStr = UnsafeMutablePointer<Int8>.allocate(capacity: strsize)
        let login = UserDefaults.standard.object(forKey: "login") as? String ?? ""
        let unsafePointerUserName = UnsafeMutablePointer<Int8>(mutating: (login as NSString ).utf8String)
        let unsafePointerLang = UnsafeMutablePointer<Int8>(mutating: ("EN" as NSString).utf8String)
        let serversStrExpires = UnsafeMutablePointer<Int8>.allocate(capacity: strsize)
        SslGetServerList(serversStr,  Int32(strsize),unsafePointerUserName, unsafePointerLang, serversStrExpires, Int32(strsize))
        return String(cString: serversStr)
    }
    
    private func parseServersString(_ string: String) -> [Server] {
        let serverLines = string.components(separatedBy: "\n")
        var servers = [Server]()
        
        for line in serverLines {
            let serverProps = line.components(separatedBy: "|")
            if serverProps.count < 4 { continue }
            
            let server = Server(country: serverProps[1], nameCode: serverProps[2], serverDomen: serverProps[3], groupName: serverProps[0])
            servers.append(server)
        }
        
        return servers
    }
}
