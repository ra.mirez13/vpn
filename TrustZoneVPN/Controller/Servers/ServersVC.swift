//
//  ServersVC.swift
//  openSSlVPN
//
//  Created by Zakhar on 7/30/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit

final class ServersVC: UIViewController {
    @IBOutlet private weak var collectionView: UICollectionView!
    static let shared = ServersVC()
    
    var value = [ServersService]()
    var selectedIndexes = [IndexPath]()
    var selectedIndexPath: IndexPath?
    let serversService = ServersService.shared
    private var cellModels = [ServerGroupCellModel]()
    var colorSelect = false
    
    private let itemsInRow: CGFloat = 4.5
    
    private var itemSize: CGFloat = .zero
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(ServerCollectionCell.cellNib, forCellWithReuseIdentifier: ServerCollectionCell.cellIdentifier)
        registerNotifications()
        setupGestures()
        updateServers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        itemSize = (collectionView.frame.size.width - itemsInRow ) / itemsInRow
        serversService.loadServerGroups()
        
        
    }
    
    private func setupGestures() {
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        view.addGestureRecognizer(leftSwipe)
        view.addGestureRecognizer(rightSwipe)
    }
    
    private func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateServers), name: serversService.serversUpdatedNoticationName, object: nil)
    }
    
    @objc private func updateServers() {
        cellModels = serversService.serverGroups.map { ServerGroupCellModel(group: $0, isExpanded: true) }
        collectionView.reloadData()
    }
    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if sender.direction == .left {
            self.tabBarController!.selectedIndex += 1
        }
        if sender.direction == .right {
            self.tabBarController!.selectedIndex -= 1
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension ServersVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return cellModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if !cellModels[section].isExpanded {
            return 0
        }
        
        return section < cellModels.count ? cellModels[section].group.servers.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
     
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ServerCollectionCell.cellIdentifier, for: indexPath)
      
        if self.selectedIndexPath != nil && indexPath == self.selectedIndexPath {
            cell.layer.backgroundColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
        }else {
            cell.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
       
        return collectionView.dequeueReusableCell(withReuseIdentifier: ServerCollectionCell.cellIdentifier, for: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? ServerCollectionCell else { return }
        let server = cellModels[indexPath.section].group.servers[indexPath.row]
        cell.setName(server.domenName.uppercased())
        cell.setFlagImage(server.flag)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                           withReuseIdentifier: ServerGroupHeaderView.identifier,
                                                                           for: indexPath) as? ServerGroupHeaderView else { return UICollectionReusableView() }
        header.setGroupName(cellModels[indexPath.section].group.name)
        header.setArrowDirection(cellModels[indexPath.section].isExpanded ? .down : .up)
        header.headerTapAction = { [weak self] headerView in
            guard let self = self else { return }
            let isExpanded = !self.cellModels[indexPath.section].isExpanded
            self.cellModels[indexPath.section].isExpanded = isExpanded
            
            self.collectionView.performBatchUpdates({
                self.collectionView.reloadSections(IndexSet(integer: indexPath.section))
            }, completion: nil)
        }
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let domenName = cellModels[indexPath.section].group.servers[indexPath.row].serverDomen
        print(domenName)
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.backgroundColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
        self.selectedIndexPath = indexPath
        DomenName.sharedInstance.name = domenName
        VPN.shared.domen = domenName
        _ = self.tabBarController?.selectedIndex = 2
        DomenName.sharedInstance.domenSelected = true
        NotificationCenter.default.addObserver(self, selector: #selector(HomeVC.domenNameDidChange(_:)), name: .dataDownloadCompleted, object: nil)
        selectedIndexes.append(indexPath)
       
        
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.selectedIndexPath = nil
        
        
    }
}

extension Notification.Name {
    static let dataDownloadCompleted = Notification.Name(
        rawValue: "domenName")
    static let animated = Notification.Name(
        rawValue: "animation")
    static let exit = Notification.Name(
        rawValue: "exit")
    
}

