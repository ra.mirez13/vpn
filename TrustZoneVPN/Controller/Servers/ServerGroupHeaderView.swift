//
//  ServerGroupHeaderView.swift
//  openSSlVPN
//
//  Created by Zakhar on 7/30/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit

enum ArrowDirection {
    case up
    case down
}

class ServerGroupHeaderView: UICollectionReusableView {
    @IBOutlet private weak var groupName: UILabel!
    @IBOutlet private weak var arrowImageView: UIImageView!
    
    static var identifier: String { return String(describing: self) }
    
    var headerTapAction: ((ServerGroupHeaderView) -> Void)?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(headerTapped))
        isUserInteractionEnabled = true
        addGestureRecognizer(tap)
    }
    
    @objc func headerTapped() {
        headerTapAction?(self)
    }
    
    func setGroupName(_ name: String) {
        groupName.text = name
    }
    
    func setArrowDirection(_ direction: ArrowDirection) {
        arrowImageView.image = UIImage(named: direction == .up ? "arrow_up" : "arrow_down")
    }
}
