//
//  PopUpVC.swift
//  openSSlVPN
//
//  Created by Konstantin Chukhas on 7/17/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit

class PopUpVC: UIViewController {
    
    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var topView: UIView!
    var value = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.labelText.text = value.htmlToString
        self.topView.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
        
        
    }
    
    
    
    @IBAction func closePopUP(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
       
        
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
