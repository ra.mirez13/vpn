//
//  ExitVC.swift
//  openSSlVPN
//
//  Created by Konstantin Chukhas on 7/18/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit
import QuartzCore
import NetworkExtension
class ExitVC: UIViewController {
   
    
    var vpnManager = NEVPNManager.shared()
    
    override func viewDidLoad() {
        super.viewDidLoad()
          NSLog("viewDidLoad is running")
        // Do any additional setup after loading the view.
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        self.view.addGestureRecognizer(leftSwipe)
        self.view.addGestureRecognizer(rightSwipe)
    }
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if sender.direction == .left {
            self.tabBarController!.selectedIndex += 1
        }
        if sender.direction == .right {
            self.tabBarController!.selectedIndex -= 1
        }
    }
    private var isConnectedToVpn: Bool {
        if let settings = CFNetworkCopySystemProxySettings()?.takeRetainedValue() as? Dictionary<String, Any>,
            let scopes = settings["__SCOPED__"] as? [String:Any] {
            for (key, _) in scopes {
                if key.contains("tap") || key.contains("tun") || key.contains("ppp") || key.contains("ipsec") {
                    return true
                }
            }
        }
        return false
    }
    
    @IBAction func OkActionButton(_ sender: Any) {
        if    isConnectedToVpn == true{
            NotificationCenter.default.addObserver(self, selector: #selector(HomeVC.animatedDidChange(_:)), name: .animated, object: nil)
            VPN.shared.disconnectVPN()
            vpnManager.connection.stopVPNTunnel()
        }
        
         exit(0)
       
    }
   
    @IBAction func cancelActionButton(_ sender: Any) {
       
    }
}
