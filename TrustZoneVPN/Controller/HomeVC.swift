//
//  HomeVC.swift
//  openSSlVPN
//
//  Created by Konstantin Chukhas on 7/18/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit
import MBCircularProgressBar
import NetworkExtension
import KeychainSwift
import Security
import MaterialActivityIndicator
import QuartzCore
import Alamofire
import SystemConfiguration
import Reachability

class HomeVC: UIViewController {
    static let shared = HomeVC()
    
    @IBOutlet weak var progressView: MBCircularProgressBarView!
    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var switchConntectionStatus: UISwitch!
    @IBOutlet weak var labelConntectionStatus: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var materialActivityIndicator: MaterialActivityIndicatorView!
    @IBOutlet weak var touchLabel: UILabel!
    @IBOutlet weak var ipLabel: UILabel!
    @IBOutlet weak var ipImage: UIImageView!
    
    
    var vpnManager = NEVPNManager.shared()
    var isConnected = false
    var buttonSwitched : Bool = false
    var networkManager = NetworkManager()
    var servLine = [String]()
    var nameImage = ""
    var sender = Int()
    var stopTap = true
    var stopVpn = false
    let reachability = Reachability()!
    private let keychain = KeychainService()
    private var isLoading = false {
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.isLoading ? self.startLoading() : self.stopLoading()
               

            }
        }
    }
    
    private let vpn = VPN.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupGestures()
        setupObservers()
        setupMaterialActivityIndicator()
        let tap = UITapGestureRecognizer(target: self, action: #selector(HomeVC.tapFunction))
        touchLabel.isUserInteractionEnabled = true
        touchLabel.addGestureRecognizer(tap)
    }
   
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        getIP()
        _ = myFunction
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getIP()
        self.reachabilityFunc()
        DomenName.sharedInstance.domenSelected = false
        _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(getIP), userInfo: nil, repeats: true)
        checkConectedToVpn()
        
    }
    
    
    private func setupGestures() {
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        self.view.addGestureRecognizer(leftSwipe)
        self.view.addGestureRecognizer(rightSwipe)
    }
    
    private func setupObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(restartVPN), name: vpn.domenChangedNotificationName, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeVC.VPNStatusDidChange(_:)), name: NSNotification.Name.NEVPNStatusDidChange, object: nil)
    }
    
    @objc private func restartVPN() {
        vpn.isConnected ? vpn.restartVPN() : vpn.connectVPN()
    }
    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if sender.direction == .left {
            self.tabBarController!.selectedIndex += 1
        }
        if sender.direction == .right {
            self.tabBarController!.selectedIndex -= 1
        }
    }
    
    private func setupMaterialActivityIndicator() {
        materialActivityIndicator.isHidden = true
        materialActivityIndicator.lineWidth =  90
        materialActivityIndicator.addGradientWithColor(color: #colorLiteral(red: 0.2509803922, green: 0.7529411765, blue: 1, alpha: 1))
        materialActivityIndicator.color = #colorLiteral(red: 0.3103698492, green: 0.7292534709, blue: 0.9948167205, alpha: 1)
        materialActivityIndicator.layer.cornerRadius = 125
    }
    
    private func startLoading() {
        materialActivityIndicator.isHidden = false
        materialActivityIndicator.startAnimating()
    }
    
    private func stopLoading() {
        materialActivityIndicator.stopAnimating()
        materialActivityIndicator.isHidden = true
    }
    
    let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
    
    func checkConectedToVpn(){
        if isConnectedToVpn == true{
            UIView.animate(withDuration: 0.5, animations: {() -> Void in
                let yourImage: UIImage =  UIImage(named: "logo_done")!
                self.img.image =  yourImage
                self.img?.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                VPNMethod.share.vpnStatusImage = true
                UserDefaults.standard.set(true, forKey: "vpnStatus")
            })
            
        } else {
            
            UIView.animate(withDuration: 0.5, animations: {() -> Void in
                let yourImage: UIImage =  UIImage(named: "ic_launcher")!
                self.img.image =  yourImage
                self.img?.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                VPNMethod.share.vpnStatusImage = false
                UserDefaults.standard.set(false, forKey: "vpnStatus")
                
            })
            
        }
    }
    private var isConnectedToVpn: Bool {
        if let settings = CFNetworkCopySystemProxySettings()?.takeRetainedValue() as? Dictionary<String, Any>,
            let scopes = settings["__SCOPED__"] as? [String:Any] {
            for (key, _) in scopes {
                if key.contains("tap") || key.contains("tun") || key.contains("ppp") || key.contains("ipsec") {
                    return true
                }
            }
            
        }
        return false
    }
    @objc func domenNameDidChange(_ notification: Notification?){
        //print("_ notification: Notification?")
        startVPN()
    }
    func startVPN(){
        DispatchQueue.main.async {
            self.vpn.connectVPN()
            self.materialActivityIndicator.addGradientWithColor(color: #colorLiteral(red: 0.2509803922, green: 0.7529411765, blue: 1, alpha: 1))
            self.materialActivityIndicator.layer.cornerRadius = 125
            self.materialActivityIndicator.startAnimating()
            self.materialActivityIndicator.rotate(duration: 3.0)
            
            self.materialActivityIndicator.color = #colorLiteral(red: 0.3103698492, green: 0.7292534709, blue: 0.9948167205, alpha: 1)
            self.materialActivityIndicator.lineWidth =  90
            self.materialActivityIndicator.layer.cornerRadius = 125
            self.materialActivityIndicator.isHidden = false
        }
        
    }
    
    private lazy var myFunction: Void = {
        // Do something once
//        getServers()

        print("This should be executed only once during the lifetime of the program")
        if UserDefaults.standard.bool(forKey: "trueAutoconnect")  == true {
            if isConnectedToVpn == true{
                print("VPN is Activeted")
            }else{
//                 startVPN()
                reachabilityFunc()
            }
        }
    }()
    
    @objc func getIP()  {
        checkConectedToVpn()
        NetworkManager.sharedInstance.aa { (result) in
            self.servLine.append(result! as! String)
            for line in self.servLine {
                let serverProps = line.components(separatedBy: "|")
                if serverProps.count < 1 { continue }
                self.ipLabel.text = serverProps[0]
                UserDefaults.standard.set(serverProps[0], forKey: "serverIp")
                let image: UIImage = UIImage(named:"\(serverProps[1].lowercased())flags")!
                self.ipImage.image = image
            }
        }
    }
    
    func theFunction(){
        struct Holder { static var called = false }
        if !Holder.called {
            Holder.called = true
            //do the thing
            
        }
        Holder.called = false
    }
    
    @objc func VPNStatusDidChange(_ notification: Notification?) {
        let status = self.vpnManager.connection.status
        switch status {
        case .connecting:
            touchLabel.text = "Touch to leave Trust.Zone"
            isLoading = true
            
            break
        case .connected:
            print("Connected")
            touchLabel.text = "Touch to leave Trust.Zone"
            checkConectedToVpn()
            isLoading = false
            
            break
        case .disconnecting:
            touchLabel.text = "Touch to enter Trust.Zone"
            isLoading = true
            break
        case .disconnected:
            print("Disconnected")
            reachabilityFunc()
            touchLabel.text = "Touch to enter Trust.Zone"
            checkConectedToVpn()
            isLoading = false
            if  UserDefaults.standard.bool(forKey: "trueAutoconnect")  == true && self.reachability.connection == .cellular{
                self.vpn.connectVPN()
            }
            break
        case .invalid:
            isLoading = false
            break
        case .reasserting:
            isLoading = false
            break
        @unknown default:
            break
        }
    }
    
    @objc func animatedDidChange(_ notification: Notification?){
        
    }
    @objc func exitDidChange(_ notification: Notification?){
        vpnManager.connection.stopVPNTunnel()
       
    }
 
    func reachabilityFunc()  {
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi  {
                if UserDefaults.standard.bool(forKey: "trueAutoconnect")  == true && self.stopVpn == false {
                    if self.isConnectedToVpn == true  {
                        print("VPN is Activeted")
                    }else{
                        self.materialActivityIndicator.startAnimating()
                       self.vpn.connectVPN()
                    }
                }
            }else if reachability.connection == .cellular  {
                if UserDefaults.standard.bool(forKey: "trueAutoconnect")  == true && self.stopVpn == false {
                    if self.isConnectedToVpn == true  {
                        print("VPN is Activeted")
                    }else{
                         self.materialActivityIndicator.startAnimating()
                        self.vpn.connectVPN()
                    }
                }
            }
        }
        reachability.whenUnreachable = { _ in
            print("Not reachable")
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    @objc
    func tapFunction(sender:UITapGestureRecognizer) {
        if vpn.isConnected {
            vpn.disconnectVPN()
        } else {
            vpn.connectVPN()
        }
    }
    @IBAction func pressButtonAction(sender: UIButton) {
        stopTap = false
        stopVpn = true
        if vpn.isConnected {
            vpn.disconnectVPN()
        } else {
            vpn.connectVPN()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
