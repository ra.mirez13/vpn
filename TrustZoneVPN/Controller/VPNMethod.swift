//
//  VPNMethod.swift
//  TrustZoneVPN
//
//  Created by Konstantin Chukhas on 8/6/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import Foundation
import UIKit
class VPNMethod {
    static let share = VPNMethod()
    var vpnStatusImage:Bool =  false 
    var vpnColor =  #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
   var  vpnStatusLabel = ""
    var serverIp = UserDefaults.standard.object(forKey: "serverIp") ?? ""
    
}
