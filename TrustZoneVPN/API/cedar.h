//
//  cedar.h
//  VPN
//
//  Created by Konstantin Chukhas on 7/10/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

#ifndef cedar_h
#define cedar_h

#include <stdio.h>


#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>
#include <time.h>
#include <unistd.h>
#include <arpa/inet.h>

#define  LOG_TAG    "sslwrapper"
#define  ALOG(...)  
#define    INFINITE            (0xFFFFFFFF)

#define    MORE(a, min_value)    ((a) >= (min_value) ? (a) : (min_value))
#define    LESS(a, max_value)    ((a) <= (max_value) ? (a) : (max_value))
#define    MAKESURE(a, b, c)        (((b) <= (c)) ? (MORE(LESS((a), (c)), (b))) : (MORE(LESS((a), (b)), (c))))
#define    MAX_SIZE 512

#ifndef TM_YEAR_MAX
#define TM_YEAR_MAX         2106
#endif
#ifndef TM_MON_MAX
#define TM_MON_MAX          1
#endif
#ifndef TM_MDAY_MAX
#define TM_MDAY_MAX         7
#endif
#ifndef TM_HOUR_MAX
#define TM_HOUR_MAX         6
#endif
#ifndef TM_MIN_MAX
#define TM_MIN_MAX          28
#endif
#ifndef TM_SEC_MAX
#define TM_SEC_MAX          14
#endif

static int ydays[] =
{
    0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365
};

#define ADJUST_TM(tm_member, tm_carry, modulus) \
if ((tm_member) < 0){ \
tm_carry -= (1 - ((tm_member)+1) / (modulus)); \
tm_member = (modulus-1) + (((tm_member)+1) % (modulus)); \
} else if ((tm_member) >= (modulus)) { \
tm_carry += (tm_member) / (modulus); \
tm_member = (tm_member) % (modulus); \
}
#define leap(y) (((y) % 4 == 0 && (y) % 100 != 0) || (y) % 400 == 0)
#define nleap(y) (((y) - 1969) / 4 - ((y) - 1901) / 100 + ((y) - 1601) / 400)
#define leapday(m, y) ((m) == 1 && leap (y))
#define monthlen(m, y) (ydays[(m)+1] - ydays[m] + leapday (m, y))

// Constant
#ifdef CPU_64

#define    MAX_VALUE_SIZE            (384 * 1024 * 1024)    // Maximum Data size that can be stored in a single VALUE
#define    MAX_VALUE_NUM            262144    // Maximum VALUE number that can be stored in a single ELEMENT
#define    MAX_ELEMENT_NAME_LEN    63        // The length of the name that can be attached to the ELEMENT
#define    MAX_ELEMENT_NUM            262144    // Maximum ELEMENT number that can be stored in a single PACK
#define    MAX_PACK_SIZE            (512 * 1024 * 1024)    // Maximum size of a serialized PACK

#else    // CPU_64

#define    MAX_VALUE_SIZE            (96 * 1024 * 1024)    // Maximum Data size that can be stored in a single VALUE
#define    MAX_VALUE_NUM            65536    // Maximum VALUE number that can be stored in a single ELEMENT
#define    MAX_ELEMENT_NAME_LEN    63        // The length of the name that can be attached to the ELEMENT
#define    MAX_ELEMENT_NUM            131072    // Maximum ELEMENT number that can be stored in a single PACK
#define    MAX_PACK_SIZE            (128 * 1024 * 1024)    // Maximum size of a serialized PACK

#endif    // CPU_64

#define    LESS(a, max_value)    ((a) <= (max_value) ? (a) : (max_value))
// Return 'a' greater than min_value
#define    MORE(a, min_value)    ((a) >= (min_value) ? (a) : (min_value))
// Examine whether the value a is between the b and c
// Adjust value 'a' to be between b and c
#define    MAKESURE(a, b, c)        (((b) <= (c)) ? (MORE(LESS((a), (c)), (b))) : (MORE(LESS((a), (b)), (c))))
// Minimum value of a and b
#define    MIN(a, b)            ((a) >= (b) ? (b) : (a))
// Maximum value of a and b
#define    MAX(a, b)            ((a) >= (b) ? (a) : (b))

#define    VALUE_INT            0        // Integer type
#define    VALUE_DATA            1        // Data type
#define    VALUE_STR            2        // ANSI string type
#define    VALUE_UNISTR            3        // Unicode string type
#define    VALUE_INT64            4        // 64 bit integer type

typedef unsigned int INT;
typedef unsigned int UINT;
typedef unsigned char UCHAR;
typedef unsigned int UINT;
typedef    unsigned long long UINT64;
typedef    unsigned int bool;
typedef    unsigned char BYTE;
typedef    unsigned short USHORT;
typedef    unsigned short WORD;
typedef signed long long INT64;
#define    true                1
#define    false                0

#define IsBigEndian() false

struct VALUE
{
    UINT Size;                // Size
    UINT IntValue;            // Integer value
    void *Data;                // Data
    char *Str;                // ANSI string
    wchar_t *UniStr;        // Unicode strings
    UINT64 Int64Value;        // 64 bit integer type
};
typedef struct VALUE VALUE;

struct LIST
{
    unsigned int num_item, num_reserved;
    void **p;
};
typedef struct LIST LIST;

struct BUF
{
    void *buf;
    unsigned int size;
    unsigned int size_reserved;
    unsigned int current;
};

struct PACK
{
    LIST *elements;            // Element list
};

struct ELEMENT
{
    char name[64];    // Element name
    UINT num_value;            // Number of values (>=1)
    UINT type;                // Type
    VALUE **values;            // List of pointers to the value
};
typedef struct ELEMENT ELEMENT;

typedef struct PACK PACK;
typedef struct BUF BUF;
typedef struct tm tm;

typedef struct SYSTEMTIME
{
    WORD wYear;
    WORD wMonth;
    WORD wDayOfWeek;
    WORD wDay;
    WORD wHour;
    WORD wMinute;
    WORD wSecond;
    WORD wMilliseconds;
} SYSTEMTIME;

void Add(LIST *o, void *p);
void FreeList(LIST *o);
BUF *NewBuf();
void FreeBuf(BUF *b);
void WriteBuf(BUF *b, void *buf, UINT size);
void AddBufStr(BUF *b, char *str);
PACK *NewPack();
void FreePack(PACK *p);
void CopyToArray(LIST *o, void *p);
void *ToArray(LIST *o);
void FreeElement(ELEMENT *e);
void FreeValue(VALUE *v, UINT type);
bool ReadPack(BUF *b, PACK *p);
UINT ReadBufInt(BUF *b);
UINT Endian32(UINT src);
UINT Swap32(UINT value);
UINT ReadBuf(BUF *b, void *buf, UINT size);
void Zero(void *addr, UINT size);
void Zerro(void *addr, UINT size);
ELEMENT *ReadElement(BUF *b);
bool ReadBufStr(BUF *b, char *str, UINT size);
VALUE *ReadValue(BUF *b, UINT type);
VALUE *NewIntValue(UINT i);
VALUE *NewInt64Value(UINT64 i);
VALUE *NewDataValue(void *data, UINT size);
VALUE *NewStrValue(char *str);
VALUE *NewUniStrValue(wchar_t *str);
void *ZerroMalloc(UINT size);
UINT UniStrSize(wchar_t *str);
void Trim(char *str);
void TrimLeft(char *str);
void TrimRight(char *str);
UINT StrCpy(char *dst, UINT size, char *src);
void Copy(void *dst, void *src, UINT size);
UINT UniStrLen(wchar_t *str);
UINT UniStrCpy(wchar_t *dst, UINT size, wchar_t *src);
void UniTrim(wchar_t *str);
void UniTrimLeft(wchar_t *str);
void UniTrimRight(wchar_t *str);
void *Malloc(UINT size);
UINT64 ReadBufInt64(BUF *b);
UINT64 Endian64(UINT64 src);
UINT64 Swap64(UINT64 value);
void *ZeroMalloc(UINT size);
ELEMENT *NewElement(char *name, UINT type, UINT num_value, VALUE **values);
bool AddElement(PACK *p, ELEMENT *e);
ELEMENT *GetElement(PACK *p, char *name, UINT type);
ELEMENT *Search(LIST *o, ELEMENT *target);
UINT CalcUtf8ToUni(BYTE *u, UINT u_size);
UINT Utf8Len(BYTE *u, UINT size);
UINT StrLen(char *str);
UINT GetUtf8Type(BYTE *s, UINT size, UINT offset);
UINT Utf8ToUni(wchar_t *s, UINT size, BYTE *u, UINT u_size);
PACK *BufToPack(BUF *b);
UINT PackGetInt(PACK *p, char *name);
UINT PackGetIntEx(PACK *p, char *name, UINT index);
UINT GetIntValue(ELEMENT *e, UINT index);
int StrCmp(char *str1, char *str2);
BUF *PackToBuf(PACK *p);
void PackAddStr(PACK *p, char *name, char *str);
void PackAddInt(PACK *p, char *name, UINT i);
void WritePack(BUF *b, PACK *p);
bool WriteBufInt(BUF *b, UINT value);
void WriteElement(BUF *b, ELEMENT *e);
bool WriteBufStr(BUF *b, char *str);
void WriteValue(BUF *b, VALUE *v, UINT type);
UINT CalcUniToUtf8(wchar_t *s);
UINT GetUniType(wchar_t c);
UINT UniToUtf8(BYTE *u, UINT size, wchar_t *s);
bool WriteBufInt64(BUF *b, UINT64 value);
bool PackGetUniStr(PACK *p, char *name, wchar_t *unistr, UINT size);
bool PackGetUniStrEx(PACK *p, char *name, wchar_t *unistr, UINT size, UINT index);
wchar_t *GetUniStrValue(ELEMENT *e, UINT index);
bool PackGetBool(PACK *p, char *name);
bool PackGetBoolEx(PACK *p, char *name, UINT index);
UINT64 PackGetInt64(PACK *p, char *name);
UINT64 PackGetInt64Ex(PACK *p, char *name, UINT index);
void SeekBuf(BUF *b, UINT offset, int mode);

void SystemToTm(tm *t, SYSTEMTIME *st);
time_t SystemToTime(SYSTEMTIME *st);
UINT64 SystemToUINT64(SYSTEMTIME *st);
void SystemToLocal(SYSTEMTIME *local, SYSTEMTIME *system);
void NormalizeTm(tm *t);
void TmToSystem(SYSTEMTIME *st, tm *t);
void TimeToTm(tm *t, time_t time);
void UINT64ToSystem(SYSTEMTIME *st, UINT64 sec64);
void TimeToSystem(SYSTEMTIME *st, time_t t);
UINT64 SystemToLocal64(UINT64 t);
time_t c_mkgmtime(tm *tm);
time_t TmToTime(tm *t);
INT64 GetTimeDiffEx(SYSTEMTIME *basetime, bool local_time);

UINT64 UniToInt64(wchar_t *str);
void UniToStrForSingleChars(char *dst, UINT dst_size, wchar_t *src);
UINT64 ToInt64(char *str);
char *CopyUniToUtf(wchar_t *unistr);

#define    LIST_DATA(o, i)        (((o) != NULL) ? ((o)->p[(i)]) : NULL)
#define    LIST_NUM(o)            (((o) != NULL) ? (o)->num_item : 0)

#endif /* cedar_h */
