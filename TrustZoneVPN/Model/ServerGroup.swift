//
//  ServerGroup.swift
//  TrustZoneVPN
//
//  Created by Konstantin Chukhas on 8/2/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import Foundation
struct ServerGroup {
    let name: String
    let servers: [Server]
}

extension ServerGroup: Equatable {
    static func == (lhs: ServerGroup, rhs: ServerGroup) -> Bool {
        return lhs.servers == rhs.servers
    }
}
