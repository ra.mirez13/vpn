//
//  App.swift
//  TrustZoneVPN
//
//  Created by Zakhar on 9/10/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import Foundation

class App {
    
    private var serversService = ServersService.shared
    
    func start() {
        serversService.loadServerGroups()
    }
}
