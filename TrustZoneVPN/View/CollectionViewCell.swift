//
//  CollectionViewCell.swift
//  openSSlVPN
//
//  Created by Konstantin Chukhas on 7/25/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    override func prepareForReuse() {
        super.prepareForReuse()
        
        //set cell to initial state here
    }
    
    @IBOutlet weak var imageCollectionView: UIImageView!
    
    
    @IBOutlet weak var labelCollectionView: UILabel!
}
