//
//  VPN.swift
//  TrustZoneVPN
//
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import NetworkExtension

class VPN {
    
    static var shared = VPN()
    
    private init() {
        NotificationCenter.default.addObserver(self, selector: #selector(statusDidChange), name: NSNotification.Name.NEVPNStatusDidChange, object: nil)
    }
    
    private lazy var username: String = { UserDefaults.standard.object(forKey: "login") as? String ?? "" }()
    private lazy var password: String = { UserDefaults.standard.object(forKey: "password") as? String ?? "" }()

    let domenChangedNotificationName = Notification.Name("domenChanged")
    var domen: String = UserDefaults.standard.string(forKey: "domen") ?? "vpn.trust.zone" {
        didSet {
            NotificationCenter.default.post(name: domenChangedNotificationName, object: nil)
            UserDefaults.standard.set(domen, forKey: "domen")
        }
    }
    
    var isConnected: Bool {
        return vpnManager.connection.status == .connected
    }
    
    private let vpnManager = NEVPNManager.shared()
    private(set) var isRestarting = false
    
    @objc func statusDidChange() {
        let status = vpnManager.connection.status
        switch status {
        case .disconnected:
            if isRestarting {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.connectVPN()
                    self.isRestarting = false
                }
            }
        default: break
        }
    }
    private var checkVpn:(Error?) -> Void{
        return { [weak self] error in
            guard let self = self else { return }
            if error != nil {
                print("Could not load VPN Configurations")
                return
            }
    let p = NEVPNProtocolIKEv2()
            p.username = self.username
            p.remoteIdentifier = "trust.zone"
            p.serverAddress = self.domen
            p.authenticationMethod = .none
            p.useExtendedAuthentication = true
            p.disconnectOnSleep = false
            p.enablePFS = true
            self.vpnManager.protocolConfiguration = p
            self.vpnManager.localizedDescription = "Trust Zone"
            self.vpnManager.isEnabled = true
            self.vpnManager.saveToPreferences(completionHandler: self.vpnSaveHandler)
    }
    }
    private var vpnLoadHandler: (Error?) -> Void {
        return { [weak self] error in
            guard let self = self else { return }
            if error != nil {
                print("Could not load VPN Configurations")
                return
            }
    
            let p = NEVPNProtocolIKEv2()
            p.username = self.username
            p.remoteIdentifier = "trust.zone"
            p.serverAddress = self.domen
            p.authenticationMethod = .none
            let kcs = KeychainService()
            kcs.save(key: "vpn_password", value: self.password)
            let passwordRef = kcs.load(key: "vpn_password")
            p.sharedSecretReference = passwordRef
            p.passwordReference = passwordRef
            p.useExtendedAuthentication = true
            p.disconnectOnSleep = false
            p.enablePFS = true
            
            self.vpnManager.protocolConfiguration = p
            self.vpnManager.localizedDescription = "Trust Zone"
            self.vpnManager.isEnabled = true
            self.vpnManager.saveToPreferences(completionHandler: self.vpnSaveHandler)
        }
    }
    
    private var vpnSaveHandler: (Error?) -> Void {
        return { (error:Error?) in
            if (error != nil) {
                print("Could not save VPN Configurations")
                return
            } else {
                do {
                    try self.vpnManager.connection.startVPNTunnel()
                } catch let error {
                    print("Error starting VPN Connection \(error.localizedDescription)")
                }
            }
        }
    }
    public func checkVPN(){
        self.vpnManager.loadFromPreferences(completionHandler: self.checkVpn)
    }
    public func connectVPN() {
        // For no known reason the process of saving/loading the VPN configurations fails.On the 2nd time it works
        self.vpnManager.loadFromPreferences(completionHandler: self.vpnLoadHandler)
    }
    
    public func disconnectVPN() {
        vpnManager.connection.stopVPNTunnel()
    }
    
    public func restartVPN() {
        isRestarting = true
        vpnManager.connection.stopVPNTunnel()
    }
    
    deinit {
//        NotificationCenter.default.addObserver(forName: <#T##NSNotification.Name?#>, object: <#T##Any?#>, queue: <#T##OperationQueue?#>, using: <#T##(Notification) -> Void#>)
    }
}
